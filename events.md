---
layout: page
title: Events
permalink: /events/
---

### Monthly Meetings

We meet on the 2nd Thursday of every month at rotating locations in and around
Edgewater from 7:00 till whenever. Our format is loose… bring a bottle or two
to share of your latest, an oldie but goodie, experiment, whatever. We
occasionally have guest speakers, surprise beers or other beer-centric
on-goings.

### Calendar

Have an event to share?
<a href="mailto:info@edgewaterfermentationsociety.org">
Email to have it added to the group calendar.
</a>

<iframe
    style="border-width: 0"
    width="100%" height="300" 
    frameborder="0" scrolling="yes"
    src="https://calendar.google.com/calendar/embed?mode=AGENDA&amp;height=300&amp;wkst=1&amp;bgcolor=%23FFFFFF&amp;src=0mviouree59mlqcchhv4if2c7s%40group.calendar.google.com&amp;color=%232952A3&amp;ctz=America%2FDenver"
></iframe>
