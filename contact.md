---
layout: page
title: Contact
permalink: /contact/
---

**President/Head Wrangler**<br/>
Ernie Bermudez<br/>
<a href="mailto:president@edgewaterfermentationsociety.org">
president@edgewaterfermentationsociety.org</a>

**Commuications/Assistant Wrangler**<br/>
Lindsey Kincade<br/>
<a href="mailto:info@edgewaterfermentationsociety.org">
info@edgewaterfermentationsociety.org</a>

**Website/Head Geek**<br/>
Jeremy Fee<br/>
<a href="mailto:webmaster@edgewaterfermentationsociety.org">
webmaster@edgewaterfermentationsociety.org</a>
