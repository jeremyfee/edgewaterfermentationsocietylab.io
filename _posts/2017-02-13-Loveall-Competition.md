---
layout: post
title: "2017 Loveall Competition"
date: 2017-02-13
categories: update
---

Not your typical competition, a hybrid people’s choice and pro judged element. Grand Prize is brew, name and serve your winning beer on Joyride Brewing’s 10 bbl system!

<img src="/images/post/2017-loveall-flyer.jpg" alt="2017 Loveall Competition Flyer"/>

All beer styles are welcome! This contest will be two-fold: 1) A People’s Choice popular vote with prizes for 1st, 2nd and 3rd place, and 2) A Best of Show judged by BJCP-certified judges, certified Cicerones and professional brewers. All entered beers will be used for each part of the contest. Judges will give you competition feedback (50 pt. scale) on your beer regardless of placement. Best of Show pro-judged beers will also have 2nd and 3rd place…with the grand prize of brewing your winning beer on Joyride’s 10 bbl system and beer served in their taproom.

Part of the Edgewater Community Festival, come show off your talents to the thirsty masses and pro judges. Winners for 1st, 2nd and 3rd places in each segment. Tapping party of 5 gallons of your winning beer for you and up to 12 friends. All proceeds to benefit Edgewater Collective’s work promoting “Thriving Schools, Thriving Community”. For more information on the Edgewater Community Festival (bands! food trucks! good times!) visit http://www.edgewatercollective.org/edgewater-community-festival/

When
: Saturday, May 13, 2017

Brewers Arrive
: 1:00pm

Public Tasting
: 2:00pm until the beer runs out!

Where
: Citizen’s Park, Edgewater, CO (map) during the Edgewater Community Festival. Entry to the festival is free, community members will be able to join the beer tasting with a \$5 entry fee.

Entries
: Minimum 5-gallon batch per entry; 2 beers maximum entry per person. All styles welcome!

Entry Fee
: $10 for the first beer, $5 for second beer. All entries include annual membership dues to join the Edgewater Fermentation Society (normally \$25/year).

Entry Deadline
: **Registration is FULL**

### Brewers

This is a rain or shine event; however, the competition and beer service we will under tents. You must register your beers by the stated deadline and please list your anticipated style(s). We know that could change, so no worries. Both kegged or bottled beers will be eligible, with 5 gallons minimum per beer. Brewers are responsible for their own transport, cooling and serving set up. Please arrive no later than 1:00 pm to set up. Not that you wouldn’t, but you are expected to pour your own beer and discuss it with participants. Get creative, lobby, sing, dance! One friend or co-brewer may help you serve the beer. They can sing and dance too. Wear a silly hat if you want.

Bottles or keg pours will be taken from entries for the Judge’s portion before the beginning of the people’s choice and may be combined in flights by style categories for ease. Hey, they’re the pros and giving us feedback, we should be nice to them.

### Sponsors

Our great sponsors below have helped promote the contest and offered up some great prizes for our winners! Whether or not you are in the competition, please take your business to these great local shops for all your beer and brewing needs!!!

<a href="https://joyridebrewing.com/">
<img src="/images/sponsor/joyride.jpg" alt="Joyride Brewing"/>
</a>

<a href="https://tomsbrewshop.com/">
<img src="/images/sponsor/toms_brew_shop.png" alt="Tom's Brew Shop"/>
</a>

<a href="https://www.rootshootmalting.com/">
<img src="/images/sponsor/root_shoot.png" alt="Root Shoot Malting Company"/>
</a>
